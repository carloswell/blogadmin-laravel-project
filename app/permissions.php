<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permissions extends Model
{
       /**
     * entity relationship between permissions table and roles
     * table, using method roles() to return the
     *  permissions belongs to many roles. 
     */
    public function roles(){
        return $this->belongsToMany(roles::class);
    }
}
