<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\articles;
use App\categories;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         // get all the articles
         $articles = articles::all();

         return view('admin/articles', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = categories::pluck('title', 'id');
        // now we can return the data with the view
        return view('admin/articles/create', compact('cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = articles::create($request->all());
        $article->categories()->attach($request->input('category'));

        return redirect('admin/articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         // get the article
    $article = articles::where('id',$id)->first();

    // if article does not exist return to list
    if(!$article)
    {
        return redirect('/admin/articles'); // you could add on here the flash messaging of article does not exist.
    }
    return view('/admin/articles/show')->withArticle($article);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

      /*
   * Secure the set of pages to the admin.
   */
    public function __construct()
    {
        $this->middleware('auth');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
