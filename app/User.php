<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
    
    *protected $casts = [
    *    'email_verified_at' => 'datetime',
     *]; 
    */
    

    public function roles(){
        return $this->belongsToMany(roles::class);
    }

     /*
    *   check if a role is attached to a user
    */
    public function hasRole($role) {
        if (is_string($role)){
            return $this->roles->contains('name', $role);
        }
            return !! $role->intersect($this->roles)->count();
        }

    /*
    *  assign a role to a user
    */
    public function assignRole($role) {
        return $this->roles()->sync(
            roles::whereName($role)->firstOrFail()
          );
    }

}
