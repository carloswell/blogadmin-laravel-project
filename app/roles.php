<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{   
    /**
     * entity relationship between role table and permissions
     * table, using method permission() t return the roles table
     * belongs to many permissions. 
     */

    protected $fillable = [
        'name',
        'label',
     
    ];
    
    public function permissions(){
        return $this->belongsToMany(permissions::class);
    }

       /**
     * method givePermissionTo() allowing the role to take permission
     * and add to the mode roles and and sync the data making sure the db entries
     * are correct.
     */

    public function givePermissionTo(Permissions $permission)
    {
        return $this->permissions()->sync($permission);
    }
}
