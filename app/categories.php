<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    protected $fillable = [
        'title',
        'detail',
    ];


    public function articles()
    {
        return $this->belongsToMany('App\articles');
    }
}
