<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['id' => 1, 'name' => "wellington",
                'email' => 'wellington@example.com',
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
            ],
        ]);
    }
}
