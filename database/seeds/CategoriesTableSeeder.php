<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['id' => 1, 'title' => "HTML", 'detail' => "html"],
            ['id' => 2, 'title' => "PHP", 'detail' => "php"],
            ['id' => 3, 'title' => "CSS", 'detail' => "css"],
          ]);
    }
}
