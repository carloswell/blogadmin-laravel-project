<?php

use Illuminate\Database\Seeder;
use App\User;
use App\categories;
use App\articles;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       /*
           * Truncate tables code
           *
           * commented out but kept in-case we need it later for something unknown at this stage.
           */


        //disable foreign key check for this connection before running seeders
        /*DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::unguard();
        
        //truncate categories before adding in data with ids that are set.
        categories::truncate();
        User::truncate();
        articles::truncate();

        User::reguard();
        //re-enable foreign key check for tuis connection
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            /*
           * Run seed files so known data is created first
           */
        $this->call(CategoriesTableSeeder::class);
        $this->call(UserTableSeeder::class);

             /*
           * run factories
           */
        factory(User::class, 50)->create();
        factory(articles::class, 5)->create();

    }
}
