<?php
  $I = new FunctionalTester($scenario);

  $I->am('admin');
  $I->wantTo('delete an article');

      // log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
   Auth::loginUsingId(1);
  // Add db test data

  // add a test user
  $I->haveRecord('users', [
      'id' => '9999',
      'name' => 'testuser1',
      'email' => 'test1@user.com',
      'password' => 'password',
  ]);

  // Add test category
  $I->haveRecord('categories', [
      'id' => '9900',
      'title' => 'category1',
      'detail' => 'category1 detail',
  ]);



  // add a test article to delete
  $I->haveRecord('articles', [
      'id' => '9000',
      'title' => 'Article 1',
      'content' => 'article 1 content',
      'slug' => 'article1',
      'user_id' => 'testuser1',
  ]);


  // add link data for article and category for the test entry
  $I->haveRecord('article_category', [
      'article_id' => '9090',
      'category_id' => '9900',
  ]);


  // Check the user is in the db and can be seen
  $I->seeRecord('articles', ['title' => 'Article1', 'id' => '9000']);


  // When
  $I->amOnPage('/admin/articles');

  // then

  // Check  the link is present - this is because there could potentially be many update links/buttons.
  // each link can be identified by the users id as name.
  $I->seeElement('a', ['name' => '9000']);
  // And
  $I->click('Delete Article1');

  // Then
  $I->amOnPage('/admin/articles');
  // And
  $I->dontSeeElement('a', ['name' => '9000']);