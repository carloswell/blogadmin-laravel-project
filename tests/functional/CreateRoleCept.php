<?php
  $I = new FunctionalTester($scenario);

  $I->am('admin');
  $I->wantTo('create a new role');

   // log in as your admin user
 // This should be id of 1 if you created your manual login for a known user first.
 Auth::loginUsingId(1);

  // When
  $I->amOnPage('/admin/users');
  $I->see('all users', 'h1');
  $I->dontSee('design');
  // And
  $I->click('Create a new role');

  // Then
  $I->amOnPage('/admin/roles/create');
  // And
  $I->see('create Roles', 'h1');
  $I->submitForm('.createroles', [
      'name' => 'design',
      'label' => 'design pages',
  
  ]);
  // Then
  $I->seeCurrentUrlEquals('/admin/roles');
  $I->see('roles', 'h1');
  $I->see('New role added!');
  $I->see('design');



  
