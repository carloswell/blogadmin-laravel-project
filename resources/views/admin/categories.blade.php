<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Categories</title>
</head>
<body>
    <h1>Categories</h1>

    <section>
    @if (isset ($categories))

        <ul>
            @foreach ($categories as $category)
                <li>{{ $category->title }}</li>
            @endforeach
        </ul>
    @else
        <p> no categories added yet </p>
    @endif
</section>

{{ Form::open(array('action' => 'CategoriesController@create', 'method' => 'get')) }}
    <div class="row">
        {!! Form::submit('Add Category', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

    
</body>
</html>