<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Roles</title>
    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<header class="column">
@include('admin/includes/adminnav')
</header>

<article class="column">
<div class="jumbotron jumbotron-fluid" >
    <h1 class="display-4">Create Roles</h1>
</div>

            <!-- form goes here -->
{!! Form::open(['url'=>'roles']) !!}

<div class="form-group">
    {!! Form::label('name', 'Name:', ['class'=> 'font']) !!}
    {!! Form::text('name', null, ['class'=> 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('label', 'Label:') !!}
    {!! Form::textarea('email', null) !!}
</div>


<div class="form-group">
    {!! Form::submit('Create Roles', ['class' => 'button']) !!}
</div>

{!! Form::close() !!}
</article>

</body>
</html>