<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All Roles</title>
    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>

 @include('admin/includes/adminnav')


 <div class="jumbotron jumbotron-fluid" >
   <h1 class="display-4">all Roles</h1> 
</div>
   <section class="column">
     @if (isset ($roles))

         <table>
             <tr>
                 <th>name</th>
                 <th>label</th>
             </tr>
             @foreach ($roles as $role)
                 <tr>
                 <td><a href="/admin/users/{{ $role->id }}" name="{{ $role->name }}">{{ $role->name }}</a></td>
                 <td><a href="/admin/users/{{ $role->id }}" label="{{ $role->label }}">{{ $role->label }}</a></td>
                     
                 </tr>
             @endforeach
         </table>
     @else
         <p>no users</p>
     @endif
 </section>
 
</body>
</html>