<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit {{ $user->name }}</title>
    <link rel="stylesheet" href="/css/app.css" /> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>

@include('admin/includes/adminnav')

<Article class="column">
<div class="jumbotron jumbotron-fluid" >
    <h1 class="display-4">Edit - {{ $user->name }}</1>
</div>
            <!-- form goes here -->
    {!! Form::model($user, ['method' => 'PATCH', 'url' => '/admin/users/' . $user->id]) !!}

    <div class="form-group">
        {!! Form::label('name', 'Username:', ['class'=> 'font-weight-bold']) !!}
        {!! Form::text('name', null, ['class'=> 'form-control']) !!}
    </div>

    <div class="form-group  font-weight-bold">
        {!! Form::label('email', 'Email Address:', ['class'=> 'font-weight-bold']) !!}
        {!! Form::textarea('email', null, ['class'=> 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('roles', 'Roles:') !!}
        @foreach($roles as $role)
            {{ Form::label($role->name) }}
            {{ Form::checkbox('role[]', $role->id, $user->roles->contains($role->id), ['id' => $role->id]) }}
        @endforeach

    </div>

    <div class="form-group">
        {!! Form::submit('Update User and Roles',  ['class' => 'button']) !!}
    </div>


    {!! Form::close() !!}
</article>
</body>
</html>