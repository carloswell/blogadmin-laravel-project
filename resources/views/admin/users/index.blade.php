<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All Users</title>
    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>

 @include('admin/includes/adminnav')


 <div class="jumbotron jumbotron-fluid" >
   <h1 class="display-4">all Users</h1> 
</div>
   <section class="column">
     @if (isset ($users))

         <table>
             <tr>
                 <th>Username</th>
                 <th>email</th>
                 <th>Permissions</th>
             </tr>
             @foreach ($users as $user)
                 <tr>
                     <td><a href="/admin/users/{{ $user->id }}" name="{{ $user->name }}">{{ $user->name }}</a></td>
                     <td> {{ $user->email }}</td>
                     <td>
                        <ul>
                          @foreach($user->roles as $role)
                             <li>{{ $role->label }}</li>
                          @endforeach
                        </ul>
                     </td>
                 </tr>
             @endforeach
         </table>
     @else
         <p>no users</p>
     @endif
 </section>
 
</body>
</html>