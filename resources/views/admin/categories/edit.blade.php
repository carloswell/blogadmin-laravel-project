<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Categories</title>
</head>
<body>
    <h1>Edit -{{$categories->title}}</h1>

    {!! Form::model($categories, ['method' => 'POST', 'url' => 'admin/' . $categories->id]) !!}
    {{ csrf_field() }}

            <div class="form-group">
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('detail', 'Detail:') !!}
                {!! Form::textarea('detail', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Update category', ['class' => 'btn btn-primary form-control']) !!}
            </div>


    {!! Form::close() !!

</body>
</html>