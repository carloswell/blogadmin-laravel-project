<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/app.css" />
    <title>Create Category</title>
</head>
<body>

<article class="row">
 <div class="jumbotron jumbotron-fluid text-center">   
<h1 class="display-4" >Create Category</h1>
</div>

{!! Form::open(array('action' => 'CategoriesController@store', 'id' => '.createcategory')) !!}
{{ csrf_field() }}      
    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <div class=" form-group">
        {!! Form::label('detail', 'Details:') !!}
        {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('createcategory', 'Createcategory:') !!}
        {!! Form::select('category[]', $cats, null,['class' => 'form-control']) !!}
        <!-- // temporary as needs to eventually pull in categories from the categories table. -->
    </div>

   <div class="form-group ">
        {!! Form::submit('Add Category', ['class' => 'success button']) !!}
    </div>
{!! Form::close() !!}
</article>
</body>
</html>